# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{pkgs, ...}: {
  imports = [
    # Include the results of the hardware scan.
    ./gram-hardware-configuration.nix
    ./shared/configuration.nix
  ];

  networking.hostName = "gram"; # Define your hostname.
  services.jellyfin = {
    enable = true;
    user = "derj";
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      vpl-gpu-rt
      intel-media-driver
    ];
  };
  environment.sessionVariables = {LIBVA_DRIVER_NAME = "iHD";};

  # services.tlp.enable = true;
  services.syncthing = {
    enable = true;
    user = "derj";
    dataDir = "/home/derj/Documents";
    configDir = "/home/derj/Documents/.config/syncthing";
  };

  users.users.derj = {
    isNormalUser = true;
    description = "Derj";
    extraGroups = ["libvirtd" "networkmanager" "wheel" "input" "uinput" "docker" "tss"];
    packages = with pkgs; [
      firefox
    ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}

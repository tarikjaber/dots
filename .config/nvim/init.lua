require("tarik")
vim.g.clipboard = {
    name = "wl-clipboard",
    copy = {
        ["+"] = "wl-copy --type text/plain",
        ["*"] = "wl-copy --type text/plain --primary"
    },
    paste = {
        ["+"] = "wl-paste --no-newline",
        ["*"] = "wl-paste --primary --no-newline"
    },
    cache_enabled = 0,
}
vim.o.clipboard = "unnamedplus"

vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")
vim.keymap.set("n", "<leader>-", ":split<CR>")
vim.keymap.set("n", "<leader>|", ":vsplit<CR>")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- lsp
vim.keymap.set("n", "<leader>q", vim.lsp.buf.code_action)
vim.keymap.set("v", "<leader>q", vim.lsp.buf.code_action)
vim.keymap.set("n", "gt", vim.lsp.buf.type_definition)

vim.keymap.set("n", "]g", vim.diagnostic.goto_next)
vim.keymap.set("n", "[g", vim.diagnostic.goto_prev)

-- https://blog.viktomas.com/graph/neovim-lsp-rename-normal-mode-keymaps/
vim.keymap.set("n", "<leader>c", function()
    local cmdId
    cmdId = vim.api.nvim_create_autocmd({ "CmdlineEnter" }, {
        callback = function()
            local key = vim.api.nvim_replace_termcodes("<C-f>", true, false, true)
            vim.api.nvim_feedkeys(key, "c", false)
            vim.api.nvim_feedkeys("0", "n", false)
            cmdId = nil
            return true
        end,
    })
    vim.lsp.buf.rename()
    vim.defer_fn(function()
        if cmdId then
            vim.api.nvim_del_autocmd(cmdId)
        end
    end, 500)
end)

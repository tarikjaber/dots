local fzf = require('fzf-lua')

vim.keymap.set('n', '<leader>f', fzf.files, {})
vim.keymap.set('n', '<leader>g', fzf.live_grep, {})
vim.keymap.set('n', '<leader>r', fzf.lsp_references,
    { noremap = true, silent = true })
vim.keymap.set('n', '<leader>s', fzf.lsp_document_symbols, {})
vim.keymap.set('n', '<leader>w', fzf.lsp_live_workspace_symbols, {})
vim.keymap.set('n', '<leader>d', fzf.diagnostics_document, {})
vim.keymap.set('n', '<leader>v', fzf.diagnostics_workspace, {})
vim.keymap.set('n', '<leader>k', fzf.keymaps, {})
vim.keymap.set('n', '<leader>m', fzf.man_pages, {})
vim.keymap.set('n', '<leader>h', fzf.help_tags, {})

vim.keymap.set("n", "[g", vim.diagnostic.goto_next)
vim.keymap.set("n", "]g", vim.diagnostic.goto_prev)

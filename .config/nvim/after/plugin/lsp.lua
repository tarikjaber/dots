local lsp_zero = require('lsp-zero')

lsp_zero.on_attach(function(_, bufnr)
    -- see :help lsp-zero-keybindings
    -- to learn the available actions
    lsp_zero.default_keymaps({ buffer = bufnr })
end)

-- vim.keymap.set('n', '<leader>fc', vim.lsp.buf.format, {})

local servers = {
    'lua_ls',
    'ts_ls',
    'cssls',
    'templ',
    'hls',
    'zls',
    'pyright',
    'astro',
    'pyright'
}

-- https://github.com/vimjoyer/nix-editor-setup-video
require("lspconfig").nixd.setup({
    cmd = { "nixd" },
    settings = {
        nixd = {
            nixpkgs = {
                expr = "import <nixpkgs> { }",
            },
            formatting = {
                command = { "alejandra" },
            },
        },
    },
})

require 'lspconfig'.astro.setup {}
require 'lspconfig'.ocamllsp.setup {}
require 'lspconfig'.svelte.setup {}
require 'lspconfig'.bashls.setup {}
require 'lspconfig'.gleam.setup {}
require 'lspconfig'.typst_lsp.setup {}
-- require 'lspconfig'.typos_lsp.setup {}
require 'lspconfig'.gopls.setup {}
require 'lspconfig'.templ.setup {}

require 'lspconfig'.elixirls.setup {
    cmd = {
        "elixir-ls"
    }
}
-- require 'lspconfig'.lexical.setup {
--     cmd = {
--         "lexical"
--     }
-- }

require 'lspconfig'.rust_analyzer.setup {
    settings = {
        ['rust-analyzer'] = {
            checkOnSave = {
                allFeatures = true,
                overrideCommand = {
                    'cargo', 'clippy', '--workspace', '--message-format=json',
                    '--all-targets', '--all-features'
                }
            }
        }
    }
}

lsp_zero.setup_servers(servers)

vim.lsp.set_log_level("debug")
vim.filetype.add({ extension = { templ = "templ" } })

vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = { "*.rs", "*.ts", "*.gleam", "*.go", "*.lua", "*.nix" },
    callback = function()
        vim.lsp.buf.format()
    end
})

vim.filetype.add({
    pattern = { [".*/hypr/.*%.conf"] = "hyprlang" },
})

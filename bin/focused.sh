#!/bin/bash
options="Trolling\nElixir\nSchool\nIslam\nPlan\nReading\nWorkflow"
focused_on=$(echo -e $options | fuzzel -p "focused on > " -d)
if [[ ! $options =~ $focused_on || $focused_on == "Trolling" ]]; then
    killall firefox
    notify-send -i dialog-information "Focus up!"
    exit
fi
current_block=$(cat /home/tarik/.config/waybar/current_block | awk '{print $2}')
notify-send -i dialog-information "Focusing on: " "$focused_on vs $current_block"

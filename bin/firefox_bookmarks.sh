# copy bookmarks folder over
bookmarks_folder=$(fd \.default\$ ~/.mozilla/firefox)

bin_dir="$HOME/bin"
cp "${bookmarks_folder}/places.sqlite" "$bin_dir"

# Path to Firefox bookmarks database
BOOKMARKS_DB="$bin_dir/places.sqlite"

# Query to extract bookmarks by title and URL
QUERY="
SELECT b.title, p.url
FROM moz_places p
JOIN moz_bookmarks b ON p.id = b.fk
"

# Declare an associative array
declare -A TITLE_TO_URL

# Populate the associative array with titles as keys and URLs as values
while IFS="|" read -r title url; do
    echo "'$title'"
    TITLE_TO_URL["$title"]="$url"
done < <(sqlite3 "$BOOKMARKS_DB" "$QUERY")

# Use fuzzel to get the selected title
SELECTION_TITLE=$(printf '%s\n' "${!TITLE_TO_URL[@]}" | fuzzel -d -p "b> ")

# Fetch the URL corresponding to that title
URL="${TITLE_TO_URL[$SELECTION_TITLE]}"

# If a URL was selected, open it
if [ -n "$URL" ]; then
    firefox "$URL"
else
    search=$SELECTION_TITLE
    if [ -n "$search" ]; then
        firefox "https://duckduckgo.com/?t=ffab&q=$search"
    fi
fi

hyprctl dispatch focuswindow "firefox"

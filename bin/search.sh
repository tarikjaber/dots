#!/usr/bin/env zsh

# Use fuzzel to get the query
QUERY=$(fuzzel -d -l 0)

# Exit if nothing is entered
[ -z "$QUERY" ] && exit 0

# Escape URL
QUERY=$(echo "$QUERY" | sed 's/"/%22/g' | sed 's/#/%23/g')

if [[ $QUERY == y\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://www.youtube.com/results?search_query=$QUERY"
elif [[ $QUERY == z\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://singlelogin.re/s/$QUERY"
elif [[ $QUERY == i\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://duckduckgo.com?t=ffab&q=$QUERY&iax=images&ia=images"
elif [[ $QUERY == s\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://sourcegraph.com/search?q=context:global+$QUERY&patternType=keyword&sm=0"
elif [[ $QUERY == n\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=$QUERY"
elif [[ $QUERY == o\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://search.nixos.org/options?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=$QUERY"
elif [[ $QUERY == r\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://flashn.vercel.app?repeatInterval=$QUERY"
elif [[ $QUERY == f\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://flashn.vercel.app?repeatInterval=$QUERY&fixedIntervals=true"
elif [[ $QUERY == c\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://github.com/search?type=code&auto_enroll=true&q=$QUERY"
elif [[ $QUERY == cr\ * ]]; then
    QUERY=${QUERY:3}
    firefox "https://github.com/search?type=code&auto_enroll=true&q=$QUERY path:*.rs"
elif [[ $QUERY == cp\ * ]]; then
    QUERY=${QUERY:3}
    firefox "https://github.com/search?type=code&q=path:/$QUERY$/"
elif [[ $QUERY == cf\ * ]]; then
    QUERY=${QUERY:3}
    firefox "https://github.com/search?type=code&auto_enroll=true&q=path:*.$QUERY"
elif [[ $QUERY == d\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=$QUERY+!"
elif [[ $QUERY == db\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=bash+$QUERY+!"
elif [[ $QUERY == g\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=github+$QUERY+!"
elif [[ $QUERY == dr\ * ]]; then
    QUERY=${QUERY:3}
    firefox "https://kagi.com/search?q=rust+$QUERY+!"
elif [[ $QUERY == gr\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=goodreads+$QUERY+!"
elif [[ $QUERY == da\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=angular+$QUERY+!"
elif [[ $QUERY == dn\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://kagi.com/search?q=neovim+$QUERY+!"
elif [[ $QUERY == z\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://singlelogin.re/s/$QUERY?extensions%5B%5D=EPUB"
elif [[ $QUERY == a\ * ]]; then
    QUERY=${QUERY:2}
    firefox "https://annas-archive.org/search?index=&page=1&q=$QUERY&ext=epub"
elif [[ $QUERY == tb\ * ]]; then
    QUERY=${QUERY:3}
    firefox "https://www.teamblind.com/search/$QUERY"
    firefox "https://www.glassdoor.com/Search/results.htm?keyword=$QUERY"
    # firefox "https://www.levels.fyi/t/software-engineer?search=$QUERY&countryId=254&country=254&yoeChoice=mid"
else
    if [ -n $QUERY ]; then
        firefox "https://kagi.com/search?q=$QUERY"
    fi
fi

# hyprctl dispatch focuswindow "firefox"

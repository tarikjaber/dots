#!/bin/zsh
cd "$(dirname "$0")"
filename="/home/tarik/org/schedule"
day_index=1
current_day=$(date +%u)
current_time=$(date +%H%M)
block_file=~/.config/waybar/current_block

while read -r line; do
    if [[ -z "$line" ]]; then
        ((day_index++))
    else
        time=$(awk '{print $1}' <<< "$line" | sed 's/^0*//')
        event=$(awk '{print $2}' <<< "$line")
        if [[ "$day_index" -eq "$current_day" && "$current_time" -eq "$time" ]]; then
            read -r next_line
            if [[ -z "$next_line" ]]; then
                next_line="2400"
            fi
            end_time=$(awk '{print $1}' <<< "$next_line")
            notify-send "$time-$end_time $event"
            echo "$end_time $event" > "$block_file"
            pkill -RTMIN+9 waybar
            break
        fi
    fi
done < "$filename"

#!/usr/bin/env zsh

# Path to the state file
STATE_FILE=~/schedule/countdown_state

# Calculate the target end time as current time + duration in seconds
end_time=$(( $(date +%s) + $1 * 60 ))

# Keep updating the countdown until the current time is less than the end time
while :; do
  # Get current time in seconds since the epoch
  current_time=$(date +%s)

  # Exit the loop if the current time is greater than the end time
  if [[ $current_time -ge $end_time ]]; then
    break
  fi

  # Calculate remaining time in seconds
  remaining_time=$(( end_time - current_time ))

  # Calculate hours, minutes, and seconds
  hours=$((remaining_time / 3600))
  mins=$(( (remaining_time % 3600) / 60))
  secs=$((remaining_time % 60))

  # Format the output based on the remaining time
  if [[ $hours -gt 0 ]]; then
    # More than 1 hour remaining, show hours, minutes, and seconds
    formatted_time=$(printf "%s %02d:%02d:%02d" $2 $hours $mins $secs)
  elif [[ $mins -gt 0 ]]; then
    # Less than 1 hour but more than 0 minutes remaining, show minutes and seconds
    formatted_time=$(printf "%s %02d:%02d" $2 $mins $secs)
  else
    # Less than 1 minute remaining, show seconds
    formatted_time=$(printf "%s 00:%02d" $2 $secs)
  fi

  # Update the state file with the current countdown value
  echo "$formatted_time" > "$STATE_FILE"
  # Send a signal to Waybar to update the module
  pkill -RTMIN+8 waybar

  # Sleep for a short amount of time (1 second)
  sleep 1
done

# After the loop, send notification and reset the state
echo "00:00" > "$STATE_FILE"
pkill -RTMIN+8 waybar


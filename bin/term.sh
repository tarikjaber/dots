program=$(printf "edit schedule\nschedule\nswitch\nupdate\nbtop\ncmus\nhome\nbin\nbrowsers" | fuzzel -d)

if [ -n "$program" ]; then
    case $program in
        "schedule")
            foot /home/tarik/code/schedule/schedule
            ;;
        "edit schedule")
            foot nvim /home/tarik/schedule/bo
            ;;
        "switch")
            foot sudo nixos-rebuild switch
            ;;
        "update")
            nix flake update ~/bin/flake.lock
            ;;
        "home")
            foot nvim /home/tarik/dots/home.nix
            ;;
        "bin")
            foot nvim /home/tarik/bin
            ;;
        "browsers")
            /home/tarik/bin/browsers.sh
            ;;
        *)
            foot "$program"
    esac
fi

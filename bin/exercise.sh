notify-send "Hourly Notification" "5 pushups\n5 backups\n5 pullups" --icon ~/images/exercise.png

seconds_left=30

while [ $seconds_left -gt 0 ]; do
    echo "$seconds_left"
    seconds_left=$((seconds_left -= 1))
    sleep 1
done

echo "Done"


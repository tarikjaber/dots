#!/usr/bin/env zsh

# Focus Firefox window
hyprctl dispatch focuswindow "firefox"

# Simulate pressing Alt+1 to focus the first tab
sleep 0.5
wtype -M alt -k 1 -m alt

# wtype -k esc
wtype -k Escape
# wtype G
# wtype gi


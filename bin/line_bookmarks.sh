# copy bookmarks folder over
bookmarks_folder=$(fd \.default\$ ~/.mozilla/firefox)

bin_dir="$HOME/bin"
cp "${bookmarks_folder}/places.sqlite" "$bin_dir"

# Path to Firefox bookmarks database
BOOKMARKS_DB="$bin_dir/places.sqlite"

# Query to extract bookmarks by title and URL
QUERY="
SELECT b.title, p.url
FROM moz_places p
JOIN moz_bookmarks b ON p.id = b.fk
"

# Populate the associative array with titles as keys and URLs as values
while IFS="|" read -r title url; do
    echo "$url"
done < <(sqlite3 "$BOOKMARKS_DB" "$QUERY")

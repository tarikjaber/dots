while :; do
    input=$(fuzzel -d -l 0)
    if [[ $input == "stop" || -z $input ]]; then
        exit
    fi
    echo $input
    notify-send -i dialog-information "You entered: " $input
done

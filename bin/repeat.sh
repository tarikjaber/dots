#!/bin/zsh
time=$(($1 * 60))
elapsed=0
clear
while :; do
    if [[ $elapsed -ge $time ]]; then
        elapsed=0
        notify-send -i dialog-information "Timer finished"
    fi
    min=$(((time - elapsed) / 60))
    sec=$(((time - elapsed) % 60))
    printf " %02d:%02d\r" $min $sec
    ((elapsed++)) 
    sleep 1
done

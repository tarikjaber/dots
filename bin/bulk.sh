#!/usr/bin/env zsh
# File path
FILE_PATH="/home/tarik/bin/groups"

# Extract the group names from the file
TAB_GROUPS=$(awk 'BEGIN{RS="\n\n"; FS="\n"} NF > 1 {print $1}' "$FILE_PATH")
echo "Direct output of awk: $(awk 'BEGIN{RS="\n\n"; FS="\n"} NF > 1 {print $1}' "$FILE_PATH")"
# Use fuzzel to select a group
SELECTED_GROUP=$(echo "$TAB_GROUPS" | fuzzel -d)

# Extract URLs under the selected group and open them in Firefox
awk -v group="$SELECTED_GROUP" 'BEGIN{RS="\n\n"; FS="\n"} $1 == group {for(i=2; i<=NF; i++) print $i}' "$FILE_PATH" | while read -r url; do
    if [[ -z "$url" ]]; then 
        continue
    fi
    firefox "$url"
done

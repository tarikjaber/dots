#!/usr/bin/env fish

while true
    date
    natpmpc -a 1 0 udp 60 -g 10.2.0.1 && natpmpc -a 1 0 tcp 60 -g 10.2.0.1
    or begin
        echo -e "ERROR with natpmpc command \a"
        break
    end
    sleep 45
end

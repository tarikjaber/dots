#!/usr/bin/env bash

firefox "$(rg https ~/org/resources.org | sd "https://" "" | sd "www." "" | fzf)"
return

# Define your resources file
file=~/org/resources.org

# Create an associative array to map titles to URLs
declare -A url_map

# Process each URL in the file
while read -r url; do
  # Fetch title from the URL
  title=$(curl -s "$url" | rg --pcre2 -o '(?<=<title>)(.*)(?=</title>)' || echo "No Title Found")

  # Store the title and URL in the array
  url_map["$title"]=$url
done < <(rg -o 'https?://[^\s]+' "$file")

# Let user select a title using fzf
selected_title=$(printf '%s\n' "${!url_map[@]}" | fzf --prompt="Select a title: ")

# Open the corresponding URL in Firefox
firefox "${url_map[$selected_title]}"

#!/bin/zsh

current_hour=$(date +%H)

if (( current_hour >= 22 )) || (( current_hour <= 6 )); then
    systemctl suspend
fi

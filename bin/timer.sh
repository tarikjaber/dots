#!/usr/bin/env zsh

# Path to the state file
STATE_FILE=~/schedule/countdown_state

# Function to display notification
notify() {
    notify-send "$1" --icon=dialog-information
}

# Function to handle pomodoro sequences
run_pomodoro() {
    local rounds=$1
    for ((i = 1; i <= rounds; i++)); do
        cdown.sh "50" "$i/$rounds  "
        notify "$i work done"
        cdown.sh "10" "$i/$rounds 󰒲 "
        [[ $i -lt rounds ]] && notify "$i break done" || notify "$rounds session pomodoro done"
    done
}

# Menu options
opts="stop\n1\n3\n5\n10\n15\n30\n60\n90\n120\n180\n240\np1\np2\np3\np4"
input=$(echo "$opts" | fuzzel -d)

# Kill previous instances of cdown.sh but exclude this script's PID
pgrep -f timer.sh | grep -v $$ | xargs -r kill -9
pkill -f cdown.sh

if [ -z "$input" ]; then
    exit 1
fi

case $input in
    p1) run_pomodoro 1 ;;
    p2) run_pomodoro 2 ;;
    p3) run_pomodoro 3 ;;
    p4) run_pomodoro 4 ;;
    stop)
        echo "00:00" > "$STATE_FILE"
        pkill -RTMIN+8 waybar
        ;;
    *)
        cdown.sh "$input" "󰥔"
        notify "Timer is done"
        ;;
esac

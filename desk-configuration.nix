# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{pkgs, ...}: {
  imports = [
    # Include the results of the hardware scan.
    ./desk-hardware-configuration.nix
    ./shared/configuration.nix
  ];

  boot.kernelParams = [
    "video=DP-2:3840x2160@144"
    "video=HDMI-A-1:1920x1080@75"
  ];

  networking.hostName = "desk"; # Define your hostname.

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.groups = {uinput = {};};

  users.users.tarik = {
    isNormalUser = true;
    description = "Tarik";
    extraGroups = ["libvirtd" "networkmanager" "wheel" "input" "uinput" "docker" "tss"];
    packages = with pkgs; [
      firefox
    ];
  };

  services.syncthing = {
    enable = true;
    user = "tarik";
    dataDir = "/home/tarik/Documents";
    configDir = "/home/tarik/Documents/.config/syncthing";
  };

  systemd.services.lact = {
    description = "AMDGPU Control Daemon";
    after = ["multi-user.target"];
    wantedBy = ["multi-user.target"];
    serviceConfig = {
      ExecStart = "${pkgs.lact}/bin/lact daemon";
    };
    enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}

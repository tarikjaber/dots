eval "$(zoxide init zsh)"
eval "$(starship init zsh)"

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000

unsetopt EXTENDED_HISTORY
unsetopt appendhistory

p() {
    zathura "$(find ~/Downloads -name '*.pdf' | fzf)"
}

# Compile print
cpr() {
    typst compile "$1.typ"
    lp "$1.pdf"
}

cpr() {
    typst compile "$1.typ"
    lp "$1.pdf"
}

_cpr_completion() {
    compadd $(fd -e typ | sd ".typ" "")
}

compdef _cpr_completion cpr

n() {
    nix-search "$1" -m 5
}

g() {
   git add .
   git commit -m "$*"
   local branch=$(git branch --show-current)
   git push origin "$branch"
}

m() {
    man "$1" | bat -l man -p
}

precmd() {
    print -On "\e]133;A\e\\"
}

bindkey -s ^f 'tmux-sessionizer\n'
# bindkey -s ^r '!!\n'

alias s="~/code/schedule/schedule"
alias ls="eza --icons=auto"
alias steam="sleep 30; steam;"
alias ro="rg Overwatch .local/share/Steam/steamapps -l | xargs rm"

export PATH=$PATH:$HOME/bin
export PATH=$PATH:$HOME/config/emacs/bin
export PATH=$PATH:$HOME/.emacs.d/bin
export PATH=$PATH:$HOME/go/bin
export FLYCTL_INSTALL="/home/tarik/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"

export GTK_THEME=Adwaita:dark
export XDG_CURRENT_DESKTOP=wlroots
alias hf='history | fzf -e'
export TERM="xterm-256color"

PATH=$PATH:~/.config/emacs/bin
PATH=$PATH:~/.cargo/bin

# opam configuration
[[ ! -r /home/tarik/.opam/opam-init/init.zsh ]] || source /home/tarik/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

. "$HOME/.local/share/../bin/env"

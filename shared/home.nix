{
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./home/hyprland.nix
    ./home/waybar.nix
    ./home/fish.nix
    (import ./home/systemd.nix {inherit pkgs;})
  ];

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.11";

  # gtk.iconTheme.package = pkgs.adwaita-icon-theme;
  gtk.iconTheme.package = pkgs.whitesur-icon-theme;
  gtk.iconTheme.name = "WhiteSur-dark";

  # Stylix
  stylix.targets.kde.enable = false;

  # Desktop
  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = {
    "image/svg" = "firefox.desktop";
    "image/svg+xml" = "firefox.desktop";
    "image/jpg" = "firefox.desktop";
    "application/pdf" = "firefox.desktop";
  };
  xdg.desktopEntries = {
    steam = {
      name = "Steam";
      genericName = "Games";
      exec = ''
        sh -c "sleep 30 && steam"
      '';
      terminal = false;
    };
    calibre = {
      name = "calibre";
      exec = ''
        sh -c "export QT_SCALE_FACTOR_ROUNDING_POLICY=RoundPreferFloor && calibre"
      '';
      icon = "/home/tarik/images/calibre.png";
      terminal = false;
    };
  };

  programs.home-manager.enable = true;
  programs.btop.enable = true;

  programs.rofi.enable = true;

  programs.fuzzel.enable = true;
  programs.fuzzel.settings = {
    main = {
      font = lib.mkForce "JetBrainsMono Nerd Font:size=14";
      horizontal-pad = 10;
      width = 45;
    };
    border = {
      radius = 5;
      width = 2;
    };
  };

  programs.zathura = {
    enable = true;
    options = {
      recolor = true;
      recolor-reverse-video = true;
    };
    mappings = {
      "h" = "navigate previous";
      "l" = "navigate next";
      "k" = "navigate previous";
      "j" = "navigate next";
      "m" = "navigate next";
    };
  };

  services.swaync.enable = true;

  programs.foot = {
    enable = true;
    settings = {
      main.font = lib.mkForce "JetBrainsMono Nerd Font:size=14";
      key-bindings = {
        scrollback-up-half-page = "Mod1+i";
        scrollback-down-half-page = "Mod1+u";
      };
    };
  };
  programs.alacritty.enable = true;
  programs.wezterm.enable = true;

  programs.atuin.enable = true;
  programs.atuin.enableFishIntegration = true;
}

{pkgs}:
pkgs.writeShellScriptBin "time_block" ''
    cd "$(dirname "$0")" || exit
    filename="/home/tarik/schedule/schedule"

    day_index=1
    current_day=$(date +%u)
    current_time=$(date +%H%M)
    block_file=/home/tarik/.config/waybar/current_block
    echo "send notification"

    declare prev_event

    while read -r line; do
        if [[ -z "$line" ]]; then
            if [[ "$day_index" -eq "$current_day" ]]; then
                prev_event_name=$(awk '{print $2}' <<< "$prev_event")
                echo "2400 $prev_event_name" > "$block_file"
                pkill -RTMIN+9 waybar
                exit
            fi
            ((day_index++))
            continue
        fi

        full_time=$(awk '{print $1}' <<< "$line")
        time=$(sed 's/^0*//' <<< "$full_time")
        event=$(awk '{print $2}' <<< "$line")

        printf "\nDay index: %s\n" $day_index
        echo "Current day: $current_day"
        echo "$current_time"
        echo "$time"

        if [[ "$day_index" -eq "$current_day" && "$current_time" -eq "$time" ]]; then
            read -r next_line
            if [[ -z "$next_line" ]]; then
                next_line="2400"
            fi
            end_time=$next_line

            notify-send "$time-$end_time $event"
            echo "$end_time $event" > "$block_file"
            pkill -RTMIN+9 waybar
            exit
        elif [[ "$day_index" -eq "$current_day" && "$current_time" -lt "$time"  ]]; then
            prev_event_name=$(awk '{print $2}' <<< "$prev_event")
            echo "$full_time $prev_event_name" > "$block_file"
            pkill -RTMIN+9 waybar
            exit
        fi

        prev_event=$line
    done < "$filename"

  # Read through everything (means it is Sunday after all events)
    prev_event_name=$(awk '{print $2}' <<< "$prev_event")
    echo "2400 $prev_event_name" > "$block_file"
    pkill -RTMIN+9 waybar
''

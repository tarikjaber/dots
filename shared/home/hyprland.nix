{
  wayland.windowManager.hyprland.enable = true;
  wayland.windowManager.hyprland.settings = {
    dwindle = {
      force_split = 2;
    };
    device = {
      name = "04ca00b1:00-04ca:00b1-touchpad";
      sensitivity = 0.10;
    };
    misc = {
      disable_hyprland_logo = true;
    };
    xwayland = {
      force_zero_scaling = true;
    };
    monitor = [
      # "DP-2, 3840x2160@144, 0x0, 1.6"
      "DP-2, 3840x2160@144, 0x0, 2.5"
      "HDMI-A-1, 1920x1080@75, 2400x0, 1"
    ];
    windowrulev2 = [
      "tile, title:(Godot)"
      "workspace 1,class:(firefox)"
      "workspace 5,class:(pavucontrol)"
      "workspace 4,class:(zathura)"
      "workspace 5,title:(Godot)"
      "workspace 7,class:(emacs)"
      "workspace 8,class:(Gimp-2.10)"
      "workspace 10,class:(com.obsproject.Studio)"
      "workspace 10,class:(floorp)"
      "bordercolor rgb(E3FEF7),fullscreen:1"
    ];
    exec-once = [
      "[workspace 2 silent] foot nvim ~/dots/configuration.nix --working-directory ~/dots"
      "[workspace 5 silent] pavucontrol"
      "[workspace 6 silent] foot tmux"
      "[workspace 7 silent] kitty -d ~/notes nvim ."
      "[workspace 10 silent] floorp"
      "[workspace special silent] foot cmus"
      "[workspace special silent] foot ~/code/schedule/schedule"
      "firefox chatgpt.com"
      # "emacs ~/org/projects.org"
      "waybar"
      "wlsunset -l 39.3 -L -84.1 -t 2300 -T 6000"
      "kmonad ~/.config/kmonad/config.kbd"
      "kmonad ~/.config/kmonad/laptop.kbd"
    ];
    workspace = [
      "1,monitor:DP-2"
      "2,monitor:DP-2"
      "3,monitor:DP-2"
      "4,monitor:DP-2"
      "5,monitor:DP-2"
      "6,monitor:HDMI-A-1"
      "7,monitor:HDMI-A-1"
      "8,monitor:HDMI-A-1"
      "9,monitor:HDMI-A-1"
      "10,monitor:HDMI-A-1"
    ];
    input = {
      accel_profile = "flat";
      follow_mouse = 1;
      sensitivity = -0.6;
      repeat_rate = 25;
      repeat_delay = 200;
      kb_layout = "us,ara";
      kb_options = "grp:alt_space_toggle ";
    };
    general = {
      gaps_in = 4;
      gaps_out = 8;
      border_size = 2;
    };
    decoration = {
      rounding = 5;
    };
    "$mod" = "SUPER";
    bindm = [
      "$mod, mouse:272, movewindow"
      "$mod, mouse:273, resizewindow"
      "$mod ALT, mouse:272, resizewindow"
    ];
    bind =
      [
        # Volume
        ", F10, exec, wpctl set-mute @DEFAULT_SINK@ toggle"
        ", F11, exec, wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 10%-"
        ", F12, exec, wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 10%+"
        "$mod, E, exec,  file=\"$(fd .pdf ~/Downloads | sed 's|/home/tarik/Downloads/||' | fuzzel -d)\" && [ -n \"$file\" ] && zathura \"/home/tarik/Downloads/$file\""
        "$mod, V, togglefloating, "
        "$mod, B, togglegroup, "
        "$mod, F, fullscreen"
        "$mod, R, fullscreen, 1"
        "$mod SHIFT, M, exec, pkill firefox && pkill floorp && pkill emacs && hyprctl dispatch exit"
        "$mod, RETURN, exec, foot -D ~"
        "$mod, D, exec, fuzzel"
        "$mod, T, exec, systemctl suspend"
        "$mod, P, exec, hyprpicker | wl-copy"
        "$mod, Q, killactive,"
        "$mod, I, exec, firefox \"https://www.duckduckgo.com?q=$(fuzzel -d -l 0) !ducky\""
        "$mod, G, exec, ~/bin/bulk.sh"
        "$mod, U, exec, ~/bin/term.sh"
        "$mod SHIFT, U, exec, foot sudo nixos-rebuild switch"
        "$mod, SPACE, exec, ~/bin/firefox_bookmarks.sh"
        "$mod, O, exec, ~/bin/search.sh"
        "$mod, N, exec, ~/bin/timer.sh"
        "$mod, A, exec, echo $(fuzzel -d -l 0 -p 'task> ') | xargs -I {} todoist q '{}'"
        "$mod, Z, exec, ~/bin/focus-first-tab.sh"
        "$mod, C, exec, swaync-client -t"
        "$mod, S, exec, grimblast --freeze copy area"
        "$mod, W, exec, grim -g '14,45 2859x1026' - | wl-copy"
        # Movements
        "$mod, H, movefocus, l"
        "$mod, L, movefocus, r"
        "$mod, J, movefocus, d"
        "$mod, K, movefocus, u"
        "$mod SHIFT, H, movewindow, l"
        "$mod SHIFT, L, movewindow, r"
        "$mod SHIFT, J, movewindow, d"
        "$mod SHIFT, K, movewindow, u"
        "$mod SHIFT,Y, movetoworkspace,special"
        "$mod, Y, togglespecialworkspace"
      ]
      ++ (
        builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
            in [
              "$mod, ${ws}, workspace, ${toString (x + 1)}"
              "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          10)
      );
  };
}

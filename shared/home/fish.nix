{
  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      fish_add_path ~/bin
      fish_add_path ~/.cargo/bin

      set -Ux MANPAGER "sh -c 'col -bx | bat -l man -p'"
      set -Ux MANROFFOPT -c
      # set -Ux GIT_DIR ".jj/repo/store/git"

      bind \cf -M insert tmux-sessionizer
      bind \cf 'commandline -i tmux-sessionizer; commandline -f execute'

      set -U fish_greeting ""
      set -g fish_key_bindings fish_vi_key_bindings
    '';
    shellAbbrs = {
      gd = "git diff";
      git = "jj";
    };
    functions = {
      pomo = ''
        echo "Running $argv[1] blocks"
        for index in (seq $argv[1])
          echo "Work $index"
          timer 50
          echo "Break $break"
          timer 10
        end
      '';
      g = ''
        git add .
        git commit -m "$argv"
        git push
      '';
      ",p" = "lp (fd . -e pdf ~/Downloads/ | fzf) -o sides=two-sided-long-edge";
      newsboat = ''
        set start 20
        set end 22

        set hour (date +"%H")

        if test $hour -ge $start -a $hour -lt $end
            command newsboat
        else
            echo "Newsboat accessible from $start:00 to $end:00."
        end
      '';
    };
  };

  programs.nix-index.enable = true;
  programs.nix-index.enableFishIntegration = true;
  programs.nix-index.enableZshIntegration = true;
  programs.nix-index.enableBashIntegration = true;

  programs.zoxide.enable = true;
  programs.zoxide.enableFishIntegration = true;

  programs.starship.enable = true;
  programs.starship.enableFishIntegration = true;
}

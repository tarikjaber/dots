{pkgs}: {
  systemd.user = {
    services.sleepAtTenPM = {
      Service = {
        ExecStart = "${pkgs.systemd}/bin/systemctl suspend";
      };
    };

    timers.sleepAtTenPM = {
      Timer = {
        OnCalendar = "22:00";
      };
      Install = {
        WantedBy = ["timers.target"];
      };
    };

    services.hourly-notify = {
      Service = {
        ExecStart = pkgs.writeShellScript "exercise.sh" ''
          "${pkgs.systemd}/bin/systemctl --user enable sleepAtTenPM.timer"
          ${pkgs.foot}/bin/foot ~/bin/exercise.sh
        '';
      };
    };

    timers.hourly-notify = {
      Timer = {
        OnCalendar = "*-*-* 08..21:00:00";
      };
      Install = {
        WantedBy = ["timers.target"];
      };
    };
  };
}

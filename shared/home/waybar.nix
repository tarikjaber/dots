{lib, ...}: {
  programs.waybar.enable = true;
  programs.waybar.style = lib.mkForce ''
    * {
        font-family: FontAwesome, "JetBrainsMono Nerd Font", Roboto, Helvetica, Arial, sans-serif;
        font-size: 15px;
    }

    #custom-countdown,
    #custom-repeat,
    #custom-block,
    #tray,
    #battery,
    #temperature,
    #cpu,
    #mpd,
    #pulseaudio,
    #memory {
        padding: 8px 12px;
    }
  '';

  programs.waybar.settings.mainBar = {
    layer = "top";
    height = 34;
    spacing = 4;
    modules-left = ["niri/workspaces" "hyprland/workspaces" "mpd" "pulseaudio" "cpu" "memory"];
    modules-center = ["clock"];
    modules-right = ["custom/countdown" "temperature" "backlight" "battery" "battery#bat2" "tray"];
    "custom/block" = {
      signal = 9;
      exec = "cat ~/schedule/current_block";
    };
    "custom/repeat" = {
      signal = 10;
      exec = "cat ~/schedule/repeat_state";
    };
    "custom/countdown" = {
      format = " {}";
      signal = 8;
      interval = 1;
      exec = "cat ~/.config/tp/time_left";
    };
    mpd = {
      format = "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ {volume}% ";
      format-disconnected = "Disconnected ";
      format-stopped = "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ";
      unknown-tag = "N/A";
      interval = 2;
      consume-icons = {
        on = " ";
      };
      random-icons = {
        off = "<span color=\"#f53c3c\"></span> ";
        on = " ";
      };
      repeat-icons = {
        "on" = " ";
      };
      single-icons = {
        on = "1 ";
      };
      state-icons = {
        paused = "";
        playing = "";
      };
      tooltip-format = "MPD (connected)";
      tooltip-format-disconnected = "MPD (disconnected)";
    };
    clock = {
      format = " {:%H:%M 󰃭 %a %d}";
      tooltip-format = "<tt><big>{calendar}</big></tt>";
    };
    cpu = {
      format = " {usage}%";
      tooltip = false;
    };
    memory = {
      format = " {}%";
    };
    temperature = {
      hwmon-path = "/sys/class/hwmon/hwmon5/temp1_input";
      critical-threshold = 80;
      format = "{icon} {temperatureC}°C";
      format-icons = ["" "" ""];
    };
    backlight = {
      format = "{icon} {percent}%";
      format-icons = ["" "" "" "" "" "" "" "" ""];
    };
    battery = {
      states = {
        warning = 30;
        critical = 15;
      };
      format = "{icon} {capacity}%";
      format-charging = "󰂄 {capacity}%";
      format-plugged = " {capacity}%";
      format-alt = "{time} {icon}";
      format-icons = ["" "" "" "" ""];
    };
    "battery#bat2" = {
      bat = "BAT2";
    };
    network = {
      format-wifi = "{essid} ({signalStrength}%) ";
      format-ethernet = "{ipaddr}/{cidr} ";
      tooltip-format = "{ifname} via {gwaddr} ";
      format-linked = "{ifname} (No IP) ";
      format-disconnected = "Disconnected ⚠";
      format-alt = "{ifname}: {ipaddr}/{cidr}";
    };
    pulseaudio = {
      format = "{icon} {volume}%  {format_source}";
      format-bluetooth = "{volume}% {icon} {format_source}";
      format-bluetooth-muted = " {icon} {format_source}";
      format-muted = "󰝟 {format_source}";
      format-source = " {volume}%";
      format-source-muted = "";
      format-icons = {
        headphone = "";
        hands-free = "";
        headset = "";
        phone = "";
        portable = "";
        car = "";
        default = ["" "" ""];
      };
      on-click = "pavucontrol";
    };
    "custom/media" = {
      format = "{icon} {}";
      return-type = "json";
      max-length = 40;
      format-icons = {
        spotify = "";
        default = "🎜";
      };
      escape = true;
      exec = "$HOME/.config/waybar/mediaplayer.py 2> /dev/null";
    };
    tray = {
      spacing = 12;
    };
  };
}
